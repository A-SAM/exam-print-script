

Sometimes 'latex_creator.py' has trouble generating a tex file for some students. If this happens, and you are unsure why, check if you find your error in the list below. If your problem does not match any of the below, please append the list with the solution, once you've found it.



*******************************************************************************
The student may have a hidden folder .../results-clean/.metadata which has files that confuse the script (use ctrl+h to see hidden folders, or ls -a if using terminal).
Remove this .metadata folder and see if this fixes the problem. In my case, I think these students were eclipse users.
The error message was:

-md5sum: metadata/.lock: No such file or directory
Traceback (most recent call last):
  File "/scratch/userdata/tandrig/A-SAM/exam-print-script/latex_creator_main.py", line 92, in <module>
    original_files=file_hashes, lecture_title=args.lecture, verbose=args.v)
  File "/scratch/userdata/tandrig/A-SAM/exam-print-script/latex_creator.py", line 366, in main
    texstr, files = create_tex(dirname, original_files=original_files, template_dir=template_dir, verbose=verbose)
  File "/scratch/userdata/tandrig/A-SAM/exam-print-script/latex_creator.py", line 297, in create_tex
    md5s = md5sum(fname)
  File "/scratch/userdata/tandrig/A-SAM/exam-print-script/latex_creator.py", line 148, in md5sum
    raise Exception("something went wrong...")
Exception: something went wrong...

Tandri

*******************************************************************************


### 
30 Aug 2021:

! Package Listings Error: Numbers none unknown.

See the Listings package documentation for explanation.
Type  H <return>  for immediate help.
 ...                                              
                                                  
l.36 ...Error{Listings}{Numbers #1 unknown}\@ehc}}

The XeLaTeX header seems to suffer from the issue reported here:
https://tex.stackexchange.com/questions/451532/recent-issues-with-lstlinebgrd-package-with-listings-after-the-latters-updates

For me, using the crude workaround proposed in the link by replacing 

\usepackage{lstlinebgrd}

by 

\makeatletter
\let\old@lstKV@SwitchCases\lstKV@SwitchCases
\def\lstKV@SwitchCases#1#2#3{}
\makeatother
\usepackage{lstlinebgrd}
\makeatletter
\let\lstKV@SwitchCases\old@lstKV@SwitchCases

\lst@Key{numbers}{none}{%
    \def\lst@PlaceNumber{\lst@linebgrd}%
    \lstKV@SwitchCases{#1}%
    {none:\\%
     left:\def\lst@PlaceNumber{\llap{\normalfont
                \lst@numberstyle{\thelstnumber}\kern\lst@numbersep}\lst@linebgrd}\\%
     right:\def\lst@PlaceNumber{\rlap{\normalfont
                \kern\linewidth \kern\lst@numbersep
                \lst@numberstyle{\thelstnumber}}\lst@linebgrd}%
    }{\PackageError{Listings}{Numbers #1 unknown}\@ehc}}
\makeatother

was sufficient to fix it.
I will not propose an update of the header file,
because the problem may be particular to my ETH desktop.

Joost
###


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Problem: pdfnup is not installed, causing the following error message

    Traceback (most recent call last):
      File "./concatenate.py", line 24, in <module>
        call([ 'pdfnup', '--nup', '2x1', pdf, '-o', nup])
      File "/usr/lib64/python2.7/subprocess.py", line 172, in call
        return Popen(*popenargs, **kwargs).wait()
      File "/usr/lib64/python2.7/subprocess.py", line 394, in __init__
        errread, errwrite)
      File "/usr/lib64/python2.7/subprocess.py", line 1047, in _execute_child
        raise child_exception
    OSError: [Errno 2] No such file or directory

Solution: use pdfjam instead, which is done as follows

In the file concatenate.py, replace line 24, i.e.

    call([ 'pdfnup', '--nup', '2x1', pdf, '-o', nup])

by

    call(['pdfjam', '--nup', '2x1', '--landscape', pdf, '--outfile', nup])

Oliver

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
