#!/usr/bin/env python2

import copy, os, sys
from pyPdf import PdfFileWriter, PdfFileReader
from subprocess import call
import glob
import re

if len(sys.argv) < 2:

    print('Please pass path to results')

else:

    results_dir = sys.argv[1]

    pdf_files = glob.glob(os.path.join(results_dir, "*.pdf"))
    is_good = lambda f: re.match(r"[0-9.]+.pdf", os.path.basename(f))
    pdf_files = [f for f in pdf_files if is_good(f)]

    nupped_pdfs = [f[:-4] + "-nup.pdf" for f in pdf_files]

    for pdf, nup in zip(pdf_files, nupped_pdfs):
        call([ 'pdfnup', '--nup', '2x1', pdf, '-o', nup])

    output = PdfFileWriter()
    output_page_number = 0
    alignment = 2  # to align on even pages

    for path in nupped_pdfs:
        # This code is executed for every file in turn
        input = PdfFileReader(open(path))
        pages = [input.getPage(i) for i in range(input.getNumPages())]
        for p in pages:
            # This code is executed for every input page in turn
            output.addPage(p)
            output_page_number += 1

        while output_page_number % alignment != 0:
            output.addBlankPage()
            output_page_number += 1

    path = os.path.join(results_dir, 'out.pdf')
    output.write(open(path, 'w'))

    pdf_in = open(path, 'r')
    pdf_reader = PdfFileReader(pdf_in)
    pdf_writer = PdfFileWriter()

    for pagenum in range(pdf_reader.numPages):
        page = pdf_reader.getPage(pagenum)
        if pagenum % 2:
            page.rotateClockwise(180)
        pdf_writer.addPage(page)

    path = os.path.join(results_dir, 'out_rotated.pdf')
    pdf_writer.write(open(path, 'w'))
